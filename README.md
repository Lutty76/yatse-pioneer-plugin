# Yatse Audio/Video Pioneer receiver plugin

Yatse Audio/Video Pioneer receiver plugins

Current version : N/A (Alpha)

Based from :

[api](https://github.com/Tolriq/yatse-avreceiverplugin-api/tree/master/api) folder contains the API to include in your project.

[sample](https://github.com/Tolriq/yatse-avreceiverplugin-api/tree/master/sample) folder contains a fully working plugin that shows everything you need to write a complete plugin.

Javadoc of the API and the sample plugin should describe everything you need to get started.

## Info

Volume Up and Down work

Set volume is Disabled

Read receiver status Work

Custom command work

Setting Port work

## Documentation

The [Wiki](https://github.com/Tolriq/yatse-avreceiverplugin-api/wiki) will contain the up to date documentation for each API versions.


/*
 * Copyright 2015 Tolriq / Genimee.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package tv.yatse.plugin.avreceiver.pioneer;

import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import tv.yatse.plugin.avreceiver.api.AVReceiverPluginService;
import tv.yatse.plugin.avreceiver.api.PluginCustomCommand;
import tv.yatse.plugin.avreceiver.api.YatseLogger;
import tv.yatse.plugin.avreceiver.pioneer.helpers.PreferencesHelper;
import tv.yatse.plugin.avreceiver.pioneer.helpers.TCPClient;

/**
 * Sample AVReceiverPluginService that implement all functions with dummy code that displays Toast and logs to main Yatse log system.
 * <p/>
 * See {@link AVReceiverPluginService} for documentation on all functions
 */
public class AVPluginService extends AVReceiverPluginService {
    private Handler handler = new Handler(Looper.getMainLooper());
    private static final String TAG = "AVPluginService";
    private static final int MAX_VOLUME = 160;

    private String mHostUniqueId;
    private String mHostName;
    private String mHostIp;
    private TCPClient mTcpClient;

    private boolean mIsMuted = false;
    private boolean mIsOn = true;    //Power status of receiver
    private double mVolumePercent = 50;

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public void onDestroy() {
        mTcpClient.stopClient();
    }
    @Override
    protected int getVolumeUnitType() {

        Boolean receiverCompatibleSetVol = Boolean.parseBoolean(PreferencesHelper.getInstance(getApplicationContext()).hostSetVol(mHostUniqueId));

        if (receiverCompatibleSetVol)
            return UNIT_TYPE_PERCENT;
        else
            return UNIT_TYPE_NONE;
    }

    @Override
    protected double getVolumeMinimalValue() {
        return 0.0;
    }

    @Override
    protected double getVolumeMaximalValue() {
        return 100.0;
    }

    @Override
    protected boolean setMuteStatus(boolean status) {
        YatseLogger.getInstance(getApplicationContext()).logVerbose(TAG, "Setting mute status : %s", status);
        if (status)
            mTcpClient.sendMessage("MO\r\n");
        else
            mTcpClient.sendMessage("MF\r\n");
        mIsMuted = status;
        return true;
    }

    @Override
    protected boolean getMuteStatus() {
        return mIsMuted;
    }

    @Override
    protected boolean toggleMuteStatus() {
        YatseLogger.getInstance(getApplicationContext()).logVerbose(TAG, "Toggling mute status");
        setMuteStatus(!mIsMuted);
        return true;
    }

    @Override
    protected boolean setVolumeLevel(double volume) {
        double lastval=-1;
        YatseLogger.getInstance(getApplicationContext()).logVerbose(TAG, "Setting volume level : %s", volume);
        mTcpClient.sendMessage(String.format("%03d", (int) volume * 2) + "VL\r\n");
        return true;
    }

    @Override
    protected double getVolumeLevel() {
        return mVolumePercent;
    }

    @Override
    protected boolean volumePlus() {
        YatseLogger.getInstance(getApplicationContext()).logVerbose(TAG, "Calling volume plus");
        mTcpClient.sendMessage("VU\r\n");
        return true;
    }

    @Override
    protected boolean volumeMinus() {
        YatseLogger.getInstance(getApplicationContext()).logVerbose(TAG, "Calling volume minus");
        mTcpClient.sendMessage("VD\r\n");
        return true;
    }

    @Override
    protected boolean refresh() {
        YatseLogger.getInstance(getApplicationContext()).logVerbose(TAG, "Refreshing values from receiver");

        mTcpClient.sendMessage("?V\r\n"); // Ask current volume to receiver
        mTcpClient.sendMessage("?M\r\n"); // Ask mute state to receiver
        mTcpClient.sendMessage("?P\r\n"); // Ask power state to receiver
        return true;
    }

    @Override
    protected List<PluginCustomCommand> getDefaultCustomCommands() {
        String source = getString(R.string.plugin_unique_id);
        List<PluginCustomCommand> commands = new ArrayList<>();
        // Plugin custom commands must set the source parameter to their plugin unique Id !
        commands.add(new PluginCustomCommand().title("Power on").source(source).param1("PO").type(0));
        commands.add(new PluginCustomCommand().title("Power off").source(source).param1("PF").type(0));
        commands.add(new PluginCustomCommand().title("Input CD").source(source).param1("01FN").type(0));
        commands.add(new PluginCustomCommand().title("Input CD-R/Tape").source(source).param1("03FN").type(0));
        commands.add(new PluginCustomCommand().title("Input DVD").source(source).param1("04FN").type(0));
        commands.add(new PluginCustomCommand().title("Input BD").source(source).param1("25FN").type(0));
        commands.add(new PluginCustomCommand().title("Input Internet Radio").source(source).param1("26FN").type(0));
        commands.add(new PluginCustomCommand().title("Input USB").source(source).param1("17FN").type(0));
        commands.add(new PluginCustomCommand().title("Input Video 1").source(source).param1("10FN").type(0));
        commands.add(new PluginCustomCommand().title("Input Video 2").source(source).param1("14FN").type(0));
        commands.add(new PluginCustomCommand().title("Input HDMI1").source(source).param1("19FN").type(0));
        commands.add(new PluginCustomCommand().title("Input HDMI2").source(source).param1("20FN").type(0));
        commands.add(new PluginCustomCommand().title("Input HDMI3").source(source).param1("21FN").type(0));
        commands.add(new PluginCustomCommand().title("Input HDMI4").source(source).param1("22FN").type(0));
        commands.add(new PluginCustomCommand().title("Input HDMI5").source(source).param1("23FN").type(0));
        commands.add(new PluginCustomCommand().title("Input HDMI6").source(source).param1("24FN").type(0));
        commands.add(new PluginCustomCommand().title("Input Change").source(source).param1("FU").type(0));
        return commands;
    }

    @Override
    protected boolean executeCustomCommand(PluginCustomCommand customCommand) {
        YatseLogger.getInstance(getApplicationContext()).logVerbose(TAG, "Executing CustomCommand : %s", customCommand.title());
        mTcpClient.sendMessage(customCommand.param1()+"\r\n");
        return false;
    }

    private void displayToast(final String message) {
        handler.post(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    protected void connectToHost(String uniqueId, String name, String ip) {
        mHostUniqueId = uniqueId;
        mHostName = name;
        mHostIp = ip;
        String receiverIp = PreferencesHelper.getInstance(getApplicationContext()).hostIp(mHostUniqueId);
        if (TextUtils.isEmpty(receiverIp)) {
            YatseLogger.getInstance(getApplicationContext()).logError(TAG, "No configuration for %s", name);
        }
        new connectTask().execute(""); // etablish TCP connection to receiver, I thinks that cause some crash

        YatseLogger.getInstance(getApplicationContext()).logVerbose(TAG, "Connected to : %s / %s ", name, mHostUniqueId);
    }

    @Override
    protected long getSettingsVersion() {
        return PreferencesHelper.getInstance(getApplicationContext()).settingsVersion();
    }

    @Override
    protected String getSettings() {
        return PreferencesHelper.getInstance(getApplicationContext()).getSettingsAsJSON();
    }

    @Override
    protected boolean restoreSettings(String settings, long version) {
        boolean result = PreferencesHelper.getInstance(getApplicationContext()).importSettingsFromJSON(settings, version);
        if (result) {
            connectToHost(mHostUniqueId, mHostName, mHostIp);
        }
        return result;
    }

    public class connectTask extends AsyncTask<String,String,TCPClient> {

        @Override
        protected TCPClient doInBackground(String... message) {

            //we create a TCPClient object and
            mTcpClient = new TCPClient(new TCPClient.OnMessageReceived() {
                @Override
                //here the messageReceived method is implemented
                public void messageReceived(String message) {
                    //this method read receiver message for update status
                    readMessage(message);
                }
            });

            String receiverIp = PreferencesHelper.getInstance(getApplicationContext()).hostIp(mHostUniqueId);
            String receiverPort = PreferencesHelper.getInstance(getApplicationContext()).hostPort(mHostUniqueId);
            if( receiverPort.equals(""))
                receiverPort= "8102";
            mTcpClient.run(receiverIp,Integer.valueOf(receiverPort));

            return null;
        }

        protected void readMessage(String values) {  //Read message of receiver and update every status
            if (values.length()==6 && values.startsWith("VOL")) {
                mVolumePercent =  Integer.valueOf(values.substring(3), 10);
            }else
                if (values.equals("MUT0"))
                    mIsMuted=true;
                else if (values.equals("MUT1"))
                    mIsMuted=false;
                else if (values.equals("PWR0"))
                    mIsOn=true;
                else if (values.equals("PWR2"))
                    mIsOn=false;
        }
    }
}
